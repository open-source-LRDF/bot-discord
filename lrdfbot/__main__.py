#!/usr/bin/env python3

from configparser import ConfigParser

from lrdfbot.bot import bot

CONFIG_PATH = 'bot.cfg'


def run():
    config = ConfigParser()
    config.read(CONFIG_PATH)
    bot.run(config['discord']['token'])


if __name__ == "__main__":
    run()
